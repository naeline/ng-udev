import {Book} from './book';

export const library: Array<Book> = [
  {
    id: '1',
    title: 'udev',
    author: 'promo2019'
  },
  {
    id: '2',
    title: 'Spider man',
    author: 'Tony Parker'
  },
  {
    id: '3',
    title: 'Harry potter',
    author: 'JK Rowlings'
  }
];
