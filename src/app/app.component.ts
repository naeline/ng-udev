import {Component} from '@angular/core';
import {Book} from './book';
import {library} from './library';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

}
